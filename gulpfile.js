"use strict"

// Load required libraries
var gulp         = require("gulp"),
    argv         = require("yargs").argv,
    jade         = require("jade"),
    plugins      = require("gulp-load-plugins")(),
    browserSync  = require("browser-sync").create(),
    runSequence  = require("run-sequence"),
    errorHandler = function (error) {
      console.log(error.toString())
    }

// Allow use of console.log in nodejs files
console.log = function(d) {
  process.stdout.write(d + '\n')
}


// Define options and paths
var siteName = "SSO Login",
    options = {
      siteName: siteName,
      compress: argv.compress || false,
      port: argv.port || 3000,
      paths: {
        bower:  "bower_components/",
        gef:    "bower_components/GEF/",
        output: "build",
        dev: {
          root: "src/",
          templates:  "src/*.jade",
          assets: {
            root:        "src/assets/",
            jade:        "src/assets/jade/**/*.jade",
            sass:        "src/assets/sass/**/*.{scss,sass}",
            js:          "src/assets/js/**/*.{js,es6,es7,es}",
            jsonConfig:  "src/assets/js/config/",
            fonts:       "src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}",
            images:      "src/assets/images/**/*.{gif,jpg,svg,png}"
          }
        },
        build: {
          root: ".build/",
          html: ".build/*.html",
          assets: {
            root:   ".build/assets/",
            css:    ".build/assets/css/",
            js:     ".build/assets/js/",
            fonts:  ".build/assets/fonts/",
            images: ".build/assets/images/",
            matrixImages: ".build/assets/css/images/",
          }
        },
        dist: {
          root: "dist/",
          html: "dist/*.html",
          assets: {
            root:   "dist/assets/",
            css:    "dist/assets/css/",
            js:     "dist/assets/js/",
            fonts:  "dist/assets/fonts/",
            images: "dist/assets/images/",
            matrixImages: "dist/assets/css/mysource_files/",
          }
        }
      },
      deployment: {
        environment: "test",
        html: argv.html || false,
        repository: {
          root: "../dec-ce.bitbucket.org/",
          deploymentLocation: "../dec-ce.bitbucket.org/" + siteName.replace(/\s+/g, "-").toLowerCase() + "/"
        },
        args: {
          args: "",
          cwd: "../dec-ce.bitbucket.org/",
          quiet: false
        }
      }
    }

// Delete everything in the "build" folder
gulp.task("clean", require("./tasks/clean")(gulp, plugins, options))

// Git repository short hash
gulp.task("revision", require("./tasks/revision")(gulp, plugins, options))

// Copy bower libraries
gulp.task("copy", require("./tasks/copy")(gulp, plugins, options, errorHandler, argv))

// Copy fonts found in the projects source files
gulp.task("fonts", require("./tasks/fonts")(gulp, plugins, options, errorHandler))

// Copy images (svg, gif, png, jpg) found in the projects source files
gulp.task("images", require("./tasks/images")(gulp, plugins, options, errorHandler, argv))

// Compile CSS (SASS)
gulp.task("compile-css", require("./tasks/compile-css")(gulp, plugins, options, errorHandler, argv, browserSync))
gulp.task("distribute-css", require("./tasks/distribute-css")(gulp, plugins, options, errorHandler))

// Compile CSS (SASS)
gulp.task("compile-css", require("./tasks/compile-css")(gulp, plugins, options, errorHandler, argv, browserSync))

// UnCSS and minify
gulp.task("distribute-css", require("./tasks/distribute-css")(gulp, plugins, options, errorHandler, argv, browserSync))

// Jade templating engine
gulp.task("jade-templates", ["revision"], require("./tasks/jade-templates")(gulp, plugins, options, errorHandler, jade))
gulp.task("jade-snippets", ["revision"], require("./tasks/jade-snippets")(gulp, plugins, options, errorHandler, jade))

// convert ECMAScript 6 & 7 code to ECMAScript 5 code
gulp.task("babel", require("./tasks/babel")(gulp, plugins, options, errorHandler))

// Web server and livereload
gulp.task("browser-sync", require("./tasks/browser-sync")(gulp, browserSync, options))




// Build Task
gulp.task("build", function(callback) {

  runSequence("clean", "copy", "jade-templates", "compile-css", "fonts", "images", "babel", callback)

});


// Start task - runs Browser Sync so things can be served
gulp.task("start", function(callback) {
  runSequence("build", "browser-sync", callback)
});

// Serve task. Uses BrowserSync to provide a local development web server with livereload capability e.g.
//
// $ gulp serve
//
// You can optionally pass the "compress" CLI parameter to serve compressed assets e.g.
//
// $ gulp serve --compress
//
gulp.task("watch", ["start"], function() {
  gulp.watch(options.paths.dev.assets.sass, ["compile-css"])
  gulp.watch(options.paths.dev.assets.js, ["babel"])
  gulp.watch(options.paths.dev.assets.fonts, ["fonts"])
  gulp.watch(options.paths.dev.assets.images, ["images"])
  gulp.watch(options.paths.dev.templates, ["jade-templates"])
  gulp.watch(options.paths.dev.assets.jade, ["jade-snippets"])

  // Watch for JS and HTML
  gulp.watch([options.paths.build.assets.js + "**/*.js", options.paths.build.html], browserSync.reload)
})

// Alias for watch
gulp.task("serve", ["watch"])

// Default run task
gulp.task("default", ["build"])


// Clean and rebuild the dist directory, compresses JS files, uncss and compress CSS
//
// FOR DISTRUTION ONLY
//
// $ gulp distribute
//
gulp.task("distribute", function(callback) {
  // Set the output directive to dist
  options.paths.output = "dist"
  options.compress = true

  // Distribute run task
  runSequence("clean", "copy", "babel", "jade-templates", "compile-css", "distribute-css", "fonts", "images", callback)
});


// Automated Deployment Tasks
// To deploy to dec-ce.bitbucket.org/gef/(test|review|prod) run the "deploy" task and pass either "test", "review" or "prod" to the "env" CLI parameter. e.g.
//
// $ gulp deploy --env=prod
//
// The deploy task will default to deploying to the test environment.
//
gulp.task("deploy", [], function(callback) {
  options.deployment.environment = "test"
  options.compress = true

  if (argv.env === "review") {
    options.deployment.environment = "review"
  } else if (argv.env !== undefined) {
    options.deployment.environment = argv.env
  }

  require("./tasks/deploy")(gulp, options, runSequence)

  options.compress = true;

  runSequence("distribute", "distribute-css", "dep-deploy", callback)
})
