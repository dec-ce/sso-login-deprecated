"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    // Fonts found in src/assets/fonts
    gulp.src(options.paths.dev.assets.fonts)
      .pipe(gulp.dest(options.paths[options.paths.output].assets.fonts))
      .on("error", errorHandler)
  }
}
