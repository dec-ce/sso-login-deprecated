"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    gulp.src([options.paths.dist.assets.css + "*.css"])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // Uncss
      .pipe(plugins.uncss({
        html: [
          options.paths.dist.html
        ],
        ignore: [
          ".gef-ie9",
          ".align-top",
        ]
      }))

      // Minify and compress with autoprefixer options passed
      .pipe(plugins.if(options.compress, plugins.cssnano({
        options: {
          autoprefixer: false
        }
      })))

      .pipe(plugins.autoprefixer({
          browsers: ["last 2 versions", "IE 9", "IE 10"],
          cascade: false
      }))

      .pipe(gulp.dest(options.paths.dist.assets.css))

  }
}
