"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv) {
  return function() {
    if (argv.cms === "matrix") {
      // Images (svg, png, gif, jpg) found in src/assets/images
      gulp.src(options.paths.dev.assets.images)
        .pipe(gulp.dest(options.paths[options.paths.output].assets.matrixImages))
        .pipe(gulp.dest(options.paths[options.paths.output].assets.images))
        .on("error", errorHandler)
    } else {
      // Images (svg, png, gif, jpg) found in src/assets/images
      gulp.src(options.paths.dev.assets.images)
        .pipe(gulp.dest(options.paths[options.paths.output].assets.images))
        .on("error", errorHandler)
    }
  }
}
