"use strict"

module.exports = function(gulp, plugins, options, errorHandler, jade) {

  return function() {
    return gulp.src(options.paths.dev.templates)

      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // use gulp-cached to only run jade on templates that have changed.
      .pipe(plugins.cached('jade-templates'))

      // Jade
      .pipe(plugins.jade({
        jade: jade,
        pretty: true,
        data: {
          revision: options.revision,
          configPath: "../js/"
        }
      }))

      .pipe(gulp.dest(options.paths[options.paths.output].root))

  }
}
