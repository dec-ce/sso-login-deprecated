"use strict"

var del = require("del")

module.exports = function(gulp, plugins, options) {
  return function(callback) {
    del(options.paths[options.paths.output].root, callback)
  }
}
