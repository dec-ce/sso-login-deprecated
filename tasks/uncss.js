"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    gulp.src([options.paths.dist.assets.css + "*.css"])
      .pipe(plugins.uncss({
        html: [
          options.paths.dist.html
        ]
      }))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.dist.assets.css))

  }
}
