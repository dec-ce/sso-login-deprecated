"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv) {
  return function() {

    // jQuery
    gulp.src(options.paths.bower + "jquery/dist/" + (options.compress ? "jquery.min.js" : "jquery.js"))
      .pipe(plugins.rename("jquery.js"))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths[options.paths.output].assets.js + "vendor"))

    // UIKit
    var uikitPaths
    if (options.compress) {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.min.js"]
    } else {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.js", "!" + options.paths.bower + "uikit/js/**/*.min.js"]
    }

    gulp.src(uikitPaths)
      .pipe(plugins.rename(function(path) {
        path.basename = path.basename.replace(".min", "")
      }))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths[options.paths.output].assets.js + "vendor/uikit"))

    // GEF CSS
    gulp.src([options.paths.gef + "dist/assets/css/core.css", options.paths.gef + "dist/assets/css/print.css"])
      .pipe(gulp.dest(options.paths[options.paths.output].assets.css))

    // GEF Fonts
    gulp.src(options.paths.gef + "dist/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}")
      .pipe(gulp.dest(options.paths[options.paths.output].assets.fonts))

    // UIKit CSS
    gulp.src(options.paths.bower + "uikit/css/uikit.css")
      .pipe(gulp.dest(options.paths[options.paths.output].assets.css))

    // Font Awesome
    gulp.src(options.paths.bower + "uikit/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}")
      .pipe(gulp.dest(options.paths[options.paths.output].assets.fonts))

    // RequireJS
    gulp.src(options.paths.bower + "requirejs/require.js")
      .pipe(plugins.if(options.compress, plugins.uglify()))
      .pipe(gulp.dest(options.paths[options.paths.output].assets.js + "vendor"))

    // Require CSS RequireJS plugin
    gulp.src(options.paths.bower + "require-css/" + (options.compress ? "css.min.js" : "css.js"))
      .pipe(plugins.rename("css.js"))
      .pipe(gulp.dest(options.paths[options.paths.output].assets.js + "vendor/requirejs-plugins/css"))

    // Config for Require
    gulp.src([options.paths.gef + "assets/js/config/config.js", options.paths.dev.assets.jsonConfig + "site-config.js"])
      .pipe(gulp.dest(options.paths[options.paths.output].assets.js))

    // prism.js syntax highlighting
    gulp.src([
      options.paths.bower + "prism/{components,plugins,themes,vendor}/**/*",
      options.paths.bower + "prism/{components,prism}.js"
    ])
    .pipe(gulp.dest(options.paths[options.paths.output].assets.js + "vendor/prism"))

    // Placeholder support for IE9
    gulp.src(options.paths.bower + "jquery-placeholder/jquery.placeholder.js")
      .pipe(gulp.dest(options.paths[options.paths.output].assets.js + "vendor/jquery-placeholder"))


    if (argv.cms === "matrix") {
      // Images (svg, png, gif, jpg) found in src/assets/images
      gulp.src(options.paths.gef + "src/assets/images/**/*.{gif,jpg,svg,png,ico}")
        .pipe(gulp.dest(options.paths[options.paths.output].assets.matrixImages))
        .pipe(gulp.dest(options.paths[options.paths.output].assets.images))
        .on("error", errorHandler)
    } else {
      // Images (svg, png, gif, jpg) found in src/assets/images
      gulp.src(options.paths.gef + "src/assets/images/**/*.{gif,jpg,svg,png,ico}")
        .pipe(gulp.dest(options.paths[options.paths.output].assets.images))
        .on("error", errorHandler)
    }

  }
}
