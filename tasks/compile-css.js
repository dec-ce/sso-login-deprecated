"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv, browserSync) {
  return function() {
    gulp.src([options.paths.dev.assets.sass])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      .pipe(plugins.sass({
        outputStyle: options.compress ? "compressed" : "expanded"
      }))

      // if flag --cms=matrix change background image sources
      .pipe(plugins.if((argv.cms === "matrix"), plugins.replace("../images/", "images/" )))

      // If the --compress flag is true then compress the CSS
      .pipe(plugins.if(options.compress, plugins.cssnano({
        options: {
          autoprefixer: false
        }
      })))

      // Append any missing vendor prefixes for CSS3 properties
      .pipe(plugins.autoprefixer({
          browsers: ["last 2 versions", "IE 9", "IE 10"],
          cascade: false
      }))

      // Write the CSS files to disk
      .pipe(gulp.dest(options.paths[options.paths.output].assets.css))

      // Inject CSS
      .pipe(browserSync.stream())
  }
}
