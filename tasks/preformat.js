"use strict"

module.exports = function(gulp, plugins, options, errorHandler, jade) {
  return function() {
    console.log("Starting preformat")

    // Prettify and Replace - converts standard html into something pretty for preformatted content
    // Wrapping tag must be <preformat-me>, and should not be <pre>
    gulp.src(options.paths.build.html)
      .pipe(plugins.jsbeautifier({indent_size: 2}))
      .pipe(plugins.replace(/<preformat-me>[\s\S]+?<\/preformat-me>/g, function(html) {

        // Remove the <preformat-me> tags so they don't get formatted
        html = html.replace(/<preformat-me>|<\/preformat-me>/g, '')

        // Get the amount of whitespace from the first line
        var count_whites = html.search(/\s\S/);

        // Remove indent from all lines
        var regexp = new RegExp(" {" + count_whites + "}", "gm")
        html = html.replace(regexp, '')

        // Replace all text content with ellipsis
        html = html.replace(/\>([\w\d ]+)\</g, '> &hellip; <')

        // Replacers object
        // key = regex value
        // value = the replacement
        var replacers = {
          '\<'      : '&lt;',
          '\>'      : '&gt;',
          '\"'      : '&quot;'
        }

        // loop through replacers and change the html to html codes
        for (var key in replacers) {
          var regexp = new RegExp(key, 'gm')
          // console.log(regexp)
          html = html.replace(regexp, replacers[key])
        }

        return '<pre><code class="language-markup">\n' + html + '</code></pre>';

      }))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.root))
      console.log("finishing preformat")
  };
}