"use strict"

var git = require("gulp-git")

module.exports = function(gulp, options, runSequence) {

  var deployment = options.deployment,
      siteName = options.siteName,
      cpGlob = deployment.html ? "./dist/**/*.*" : "./dist/assets/**/*.*"

  deployment.repository.deploymentLocation += options.deployment.environment
  deployment.repository.deploymentLocation = deployment.html ? options.deployment.repository.deploymentLocation : options.deployment.repository.deploymentLocation += "/assets/"

  // Main deploy task
  gulp.task("dep-deploy", function() {
    git.pull("origin", "master", deployment.args, function(err) {
      if (err) {
        throw err
      } else {
        runSequence("dep-cp", "dep-add", "dep-commit", "dep-push")
      }
    });
  });

  // Copy the files
  gulp.task("dep-cp", function() {
    return gulp.src(cpGlob)
      .pipe(gulp.dest(deployment.repository.deploymentLocation))
      console.log("Copied!")
  });

  // Add any new files to the repo
  gulp.task("dep-add", function() {
    return gulp.src("./.", {cwd: deployment.repository.root})
      .pipe(git.add({cwd: deployment.repository.root}, function(err) {
        if (err) {
          throw err
        } else {
          console.log('Added new files!')
        }
      }))
  });

  // Automated commit message
  gulp.task("dep-commit", function() {
    return gulp.src("./", {cwd: deployment.repository.root})

      .pipe(git.commit("Automated deployment to " + siteName + " | " + deployment.environment + " repository", {cwd: deployment.repository.root})).on("error", function() {
        this.emit("end");
        console.log("The " + deployment.environment + " environment is already up to date. Nothing to be added :D");
      })

  })

  // Push that code.
  gulp.task("dep-push", function() {
    git.push("origin", "master", deployment.args)
    console.log("You've successfully updated the " + deployment.environment + " environment!")
    this.emit("end")
  })


}
