"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    return gulp.src([options.paths.dev.assets.js, options.paths.gef + "src/assets/js/**/*.{js,es6,es7,es}"])

      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // only change the files that have changed
      .pipe(plugins.cached('babelling'))

      .pipe(plugins.babel({
        modules: "amd"
      }))

      .pipe(plugins.if(options.compress, plugins.uglify()))

      .pipe(gulp.dest(options.paths[options.paths.output].assets.js))
  }
}
