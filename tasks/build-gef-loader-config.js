"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {

    // get the require config
    var gefConfig  = require("../" + options.paths.gef + "src/assets/js/config/config.json"),
        siteConfig = require("../" + options.paths.dev.assets.jsonConfig + "config.json")

    // get node"s extend utility
    var extend = require("util")._extend


    // combine the components
    var components = extend(gefConfig.components, siteConfig.components)

    // Add the new components to the config
    gefConfig.components = components
    var str = JSON.stringify(gefConfig)

    // get node"s writeFile method
    var fs = require("fs")

    // write the file to disk
    fs.writeFile(options.paths[options.paths.output].assets.js + "config.json", str)
  }
}
